import { createStore } from 'vuex'

let store = createStore({
  state: {
    username:""
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
export default store
