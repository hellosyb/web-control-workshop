import { createRouter, createWebHistory } from 'vue-router'
import Home from "../components/Home"
import Login from "../components/Login"
import Mybutton from "../components/Mybutton"
import Myinput from "../components/Myinput"
import Mytext from "../components/Mytext"
import Clt from "../components/Clt"
import Guide from "../components/Guide"
// import store from "../store/index.js"

const routes = [
  {
    path: "/",
    component: Home,
    meta: {
      showmenu: false
    }
  },
  {
    path: "/login",
    component: Login,
    meta: {
      showmenu: false
    }
  }, 
  {
    path: "/clt",
    component: Clt,
    meta: {
      showmenu: false
    }
  },
  {
    path: "/guide",
    component: Guide,
    meta: {
      showmenu: false
    }
  },
  {
    path: "/mybutton",
    component: Mybutton,
    meta: {
      showmenu: true
    }
  },
  {
    path: "/myinput",
    component: Myinput,
    meta: {
      showmenu: true
    }
  },
  {
    path: "/mytext",
    component: Mytext,
    meta: {
      showmenu: true
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to,from) => {

  let name = window.localStorage.getItem('curname')
  if (to.path == "/mybutton" && from.path == "/"){
    console.log('ok')
  }
  // console.log(window.localStorage,window.localStorage.getItem(name),window.localStorage.getItem('u1'))
  //如果在没有token的情况下想跳转到其他路由，将自动转移到/login
  else if (to.path != "/login" && to.path != "/" && window.localStorage.getItem(name) == null) {
    router.push({ path: "/login" })
  }
  //如果在有token的情况下访问/login，将自动转移到首页
  // if (to.path == "/login" && window.localStorage.getItem(name)) {
  //   console.log(window.localStorage)
  //   router.push({path:"/mybutton"})
  // }
})
export default router
