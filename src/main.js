// import './plugins/axios'
import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import installElementPlus from './plugins/element'
import * as ElIconModules from '@element-plus/icons'
import store from './store'

const app = createApp(App).use(store)

// 统一注册el-icon图标
for(let iconName in ElIconModules){
    let iconName1 = iconName.replace(iconName[0],iconName[0].toLowerCase());
    app.component(iconName1,ElIconModules[iconName])

}
installElementPlus(app)
app.use(router).mount('#app')