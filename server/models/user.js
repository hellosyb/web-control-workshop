const mongoose = require('mongoose')
const Schema = mongoose.Schema

let userSchema = new Schema({
    username: String,
    password: String
},{versionKey:false})

let userModel = mongoose.model('users',userSchema)

module.exports = userModel