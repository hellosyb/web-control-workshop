const mongoose = require('mongoose')
const Schema = mongoose.Schema

let cltSchema = new Schema({
    username: String,
    type: String,
    html: String,
    css: Object,
    displaycss: Object
},{versionKey:false})

let cltModel = mongoose.model('store',cltSchema)

module.exports = cltModel