const express = require('express')
const router = express.Router()
const cltModel = require('../models/clt')

//展示收藏
router.get("/", (req, res, next) => {
    let username = req.query.username
    cltModel.find({username: username}, function (error, doc) {
        if (error) {
            res.end('fail');
        } else {
            res.end(JSON.stringify(doc));
        }
    })
})

//添加收藏
router.get("/add", (req, res, next) => {
    let paras = req.query 
    paras.css = JSON.parse(paras.css)
    paras.displaycss = JSON.parse(paras.displaycss)
    cltModel.create(paras, function (error, doc) {
        if (error) {
            res.end('fail');
        } else {
            console.log(doc)
            res.end('ok');
        }
    })
})

//删除收藏
router.get("/del", (req, res, next) => {
    let dropid = req.query.dropid
    let username = req.query.username
    console.log(username)
    cltModel.remove({_id: dropid}, function (error, doc) {
        if (error) {
            res.end('fail');
        } else {
            cltModel.find({username: username}, function (error, doc) {
                if (error) {
                    res.end('fail');
                } else {
                    console.log(doc)
                    res.end(JSON.stringify(doc));
                }
            })
        }
    })
})
//批量删除收藏
router.get("/delmany", (req, res, next) => {
    let dropids = req.query.dropids
    let username = req.query.username
    console.log(dropids,username)
    cltModel.deleteMany({_id: {$in: dropids}}, function (error, doc) {
        if (error) {
            res.end('fail');
        } else {
            cltModel.find({username: username}, function (error, doc) {
                if (error) {
                    res.end('fail');
                } else {
                    console.log(doc)
                    res.end(JSON.stringify(doc));
                }
            })
        }
    })
})
module.exports = router