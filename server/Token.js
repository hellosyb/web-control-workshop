const jwt = require('jsonwebtoken')
Token = {
    encrypt(str){
        let mytoken = jwt.sign(str,'syb')
        return mytoken
    },
    decrypt(token){
        try{
            let mytoken = jwt.verify(token,'syb')
            return {
                status: 'ok',
                mytoken
            }
        }
        catch{
            return {
                status: 'fail'
            }
        }
    }
}
// console.log(Token.encrypt('小米'))
// console.log(Token.decrypt('eyJhbGciOiJIUzI1NiJ9.5bCP57Gz.f_skAGs8pMTNgtIXpPaP_K--v0iG2mN09CdscvpCOoU'))

module.exports = Token