const express = require('express')
const login = require('./routes/login')
const store = require('./routes/save')
const db = require('./db.js')
const app = express()
const port = 8082


app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Authorization,X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method')
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PATCH, PUT, DELETE')
  res.header('Allow', 'GET, POST, PATCH, OPTIONS, PUT, DELETE')
  next();
});

app.use('/login', login)
app.use('/store', store)


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})