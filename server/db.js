const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/wd',{
    useNewUrlParser: true 
})

const db = mongoose.connection
db.once('open' ,() => {
	console.log('连接数据库'+ db.name +'成功')
})

db.on('error', function(error) {
    console.error('Error in MongoDb connection: ' + error);
    mongoose.disconnect();
});

module.exports = db
