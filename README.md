## web控件工坊

**一款支持自定义风格控件的设计工具**

### 无需调试CSS

可通过拖动滑块/可视化选择的方式自定义控件的字体颜色/背景/内边距/阴影样式等各项参数，从而无需通过一遍遍调试css代码来确定满意的样式。

调至想要的样式后可一键复制HTML/CSS代码，提高开发效率。

![image-20220220211558925](https://gitee.com/hellosyb/web-control-workshop/raw/master/imgs/img1.png)

### 风格自定义

避免了使用组件库时，某些默认样式不符合需要且修改困难的情况。

### 一键保存设计

在登录/注册后，您可以将已调试满意的样式代码保存进入收藏夹内，以便下次使用。

收藏夹内默认按照控件的类型分类，并支持批量删除管理功能。

![image-20220220212629815](https://gitee.com/hellosyb/web-control-workshop/raw/master/imgs/img2.png)



### 

